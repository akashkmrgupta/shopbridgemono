## Backend code for ShopBridge application

# Setting up DB
1. SQL DB Script has been attached to the root of this respository,
2. Run the script in SSMS.

# Running the project
1. Open the project in Visual Studio (2015 or higher),
2. Restore the packages,
3. Setup relevant SQL Server details in Web.Config of the backend Project.
4. Run the project.

# If Error: Could not find a part of the path … bin\roslyn\csc.exe
1. Run Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r in package manager console.
