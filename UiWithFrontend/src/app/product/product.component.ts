import { Component, OnInit } from '@angular/core';  
import { Product } from "../product";  
import { ProductinventoryService } from "../productinventory.service";  
import { Observable } from "rxjs";  
import { Router } from '@angular/router'; 
import { PaginationInstance } from 'ngx-pagination';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {  
  public pro: any;  
  massage:String;  
  dataSaved=false; 
  filterCriteria ;
  maxSize = 9;

  constructor(private router: Router,private productinventoryService:ProductinventoryService) { }  
   Loadproduct()  
   {  
      this.productinventoryService.GetProductRecord().subscribe(
        (response) => {
          this.pro = response;
        }
      );  
   }  

   viewproduct(prd: any) {
    this.router.navigate(['/viewproduct'], {queryParams: {'product': prd}});
   }

   ProductEdit(id: string) {  
    this.router.navigate(['/addproduct', id]);  
  }  

  search(event){
    this.filterCriteria=event.target.value;
  }

   Deleteproduct(id: string) {  
    if (confirm("Are you sure you want to remove this Product from inventory?")) {  

      this.productinventoryService.DeleteProduct(id).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.massage = "Deleted Successfully"; 
          this.Loadproduct(); 
        }  
      );  
    }  
  }  
  
config: PaginationInstance = {
    itemsPerPage: 5,
    currentPage: 1
  };

onPageChange(number: number) {
    this.config.currentPage = number;
  }

  ngOnInit() {  
    localStorage.clear();
    this.Loadproduct();  
  }  
}  