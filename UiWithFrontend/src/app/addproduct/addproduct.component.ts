import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { FormGroup, FormControl } from '@angular/forms';
import { ProductinventoryService } from "../productinventory.service";
import { Product } from "../product";
import { Observable } from "rxjs";
import { identifierModuleUrl } from '@angular/compiler';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';


@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  message: string;
  dataSaved = false;
  AddProduct: FormGroup;
  ProductIdUpdate = "0";
  source: string = '';
  IsAdded: boolean = false;
  id: any = "";
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  base64: string;
  isEdit: boolean = false;

  constructor(private router: Router, private productinventoryService: ProductinventoryService, private activateroute: ActivatedRoute) {
    this.activateroute.params.subscribe(params => {
      this.id = params['id'];
      this.ProductEdit(this.id);
    });
  }

  InsertProduct(product: Product) {
    if (this.ProductIdUpdate != "0")
      product.ProductId = this.ProductIdUpdate;
      product.base64Image = this.base64;
    this.productinventoryService.InsertProduct(product).subscribe(
      () => {
        if (this.ProductIdUpdate == "0") {
          this.IsAdded = true;
          this.clearform();
          this.message = 'Saved Successfully';
        }
        else {
          this.message = 'Update Successfully';
        }
        this.dataSaved = true;
        this.router.navigate(['/']);
      })
  }
  onFormSubmit() {
    const Pro = this.AddProduct.value;
    this.InsertProduct(Pro);
  }

  ProductEdit(id: string) {
    this.productinventoryService.GetProductById(id).subscribe(pro => {
      this.message = null;
      this.dataSaved = false;
      this.ProductIdUpdate = id;
      this.AddProduct.controls['Name'].setValue(pro.Name);
      this.AddProduct.controls['Description'].setValue(pro.Description);
      this.AddProduct.controls['Price'].setValue(pro.Price);
    });
  }

  clearform() {
    this.AddProduct.controls['Name'].setValue("");
    this.AddProduct.controls['Description'].setValue("");
    this.AddProduct.controls['Price'].setValue("");
    this.isImageSaved = false; 
    this.cardImageBase64 = null;   
  }

  ngOnInit() {
    this.AddProduct = new FormGroup({
      Name: new FormControl(),
      Description: new FormControl(),
      Price: new FormControl(),
    });
    let Id = localStorage.getItem("id");
    if (Id != null) {
      this.ProductEdit(Id);
    }
    
    if(this.id)
    {
      this.isEdit = true;
    }
  }

  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {

      const max_size = 20971520;
      const allowed_types = ['image/png', 'image/jpeg'];
      const max_height = 15200;
      const max_width = 25600;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( JPG | PNG )';
        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        this.base64 = e.target.result; 
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          if (img_height > max_height && img_width > max_width) {
            this.imageError =
              'Maximum dimentions allowed ' +
              max_height +
              '*' +
              max_width +
              'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;
            this.cardImageBase64 = imgBase64Path;
            this.isImageSaved = true;
          }
        };
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }
}




