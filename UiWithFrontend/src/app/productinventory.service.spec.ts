import { TestBed } from '@angular/core/testing';

import { ProductinventoryService } from './productinventory.service';

describe('ProductinventoryService', () => {
  let service: ProductinventoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductinventoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
