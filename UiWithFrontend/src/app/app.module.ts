import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';  
import { AppRoutingModule } from './app-routing.module';  
import { ReactiveFormsModule } from "@angular/forms";  

import { HttpClientModule,HttpClient} from '@angular/common/http';   


import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ProductinventoryService } from "../app/productinventory.service";  

import { NgxPaginationModule } from 'ngx-pagination';   
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ViewproductComponent } from './viewproduct/viewproduct.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    AddproductComponent,
    ViewproductComponent
  ],
  imports: [
    BrowserModule,FormsModule, AppRoutingModule,HttpClientModule,ReactiveFormsModule,
    Ng2SearchPipeModule,NgxPaginationModule 
  ],
  providers: [ProductinventoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
