# ShopBridge

This project has three components 1. For listing products, 2. For editing/adding produts, 3. For Viewing Products.

## Node Package Installation
*node is required to run the project.
Run `npm install` for downloading the dependencies.

## Running the application

Run `ng serve -o` to open a the application. Navigate to `http://localhost:4200/`.

## Suitable Environment
1. Machine should have primarymemory of 4GB or above,
2. Alignments Work Well in 15.1" screens
3. Recomended Browser: Google Chrome, Google Canary.

## Contact
If you face any problem or if you have any enhancements, feel free to contact or push your code in this Github account.

